package relogio;


public class Horario {
	
	private byte hora, minuto, segundo;
	private int segundos;

	public byte getHora() {
		return hora;
	}

	public void setHora(int hora) {
		if(segundos >= 0 && segundos <= 86399) {
			this.hora = (byte)hora;
		}
	}

	public byte getMinuto() {
		return minuto;
	}

	public void setMinuto(int minuto) {
		if(segundos >= 0 && segundos <= 3600) {
			this.minuto = (byte)minuto;
		}
	}

	public byte getSegundo() {
		return segundo;
	}

	public void setSegundo(int segundo) {
		if(segundos >= 0 && segundos <=59) {
			this.segundo = (byte)segundo;
		}
	}
	
	public String toString() {
		return this.getHora() + ":" + this.getMinuto() + ":" + this.getSegundo() + "\t\t\t-> " + segundos;
	}

	public void incrementaSegundo() {
		int s = segundo + 1;
		segundos = segundos + 1;
		if (s == 60) {
			segundo = 0;
			incrementaMinuto();
		}else {
			segundo = (byte)s;
		}
	}
	
	public void incrementaMinuto() {
		int s = segundos / 60;
		int m = 0;
		int min = s-m;
		if (min == 60) {
			m = m+60;
			min = 0;
			incrementaHora();
		} else {
			minuto = (byte)min;
		}
	}
	
	public void incrementaHora() {
		int h = segundos / 3600;
		if (h == 24) {
			System.out.println("Acabou o dia!");
		}else {
			hora = (byte)h;
		}
	}
	
	public boolean ehUltimoSegundo() {
		return segundos == 86399;
	}
	
}
	